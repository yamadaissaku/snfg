# annotated glycans in PDB have WURCS

## Therefore, I tried to generate the image by the following procedure.



## Get WURCS of glycan structure from PDB

* EP: https://sparql.glyconavi.org/sparql
   * 1759 entries

```
SELECT count (DISTINCT ?wurcs ) as ?count
FROM <http://glyconavi.org/tcarp>
WHERE {
?pdb_entry <http://purl.org/dc/terms/identifier> ?id .
?pdb_entry <http://glyconavi.org/ontology/pdb#has_branch_entry> ?branch_entry .
?branch_entry <http://purl.jp/bio/12/glyco/glycan#has_glycan> ?glycan .
?branch_entry <http://glyconavi.org/ontology/pdb#has_entity_id> ?entity .
?glycan <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://glyconavi.org/ontology/pdb#WURCSStandard> .
?glycan <http://purl.jp/bio/12/glyco/glycan#has_glycosequence> ?glycosequence .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#has_sequence> ?wurcs .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format> <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> .
}

```


## select WURCS strings
   * (Sep-26-2022)
```
SELECT DISTINCT ?wurcs
FROM <http://glyconavi.org/tcarp>
WHERE {
?pdb_entry <http://purl.org/dc/terms/identifier> ?id .
?pdb_entry <http://glyconavi.org/ontology/pdb#has_branch_entry> ?branch_entry .
?branch_entry <http://purl.jp/bio/12/glyco/glycan#has_glycan> ?glycan .
?branch_entry <http://glyconavi.org/ontology/pdb#has_entity_id> ?entity .
?glycan <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://glyconavi.org/ontology/pdb#WURCSStandard> .
?glycan <http://purl.jp/bio/12/glyco/glycan#has_glycosequence> ?glycosequence .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#has_sequence> ?wurcs .
?glycosequence <http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format> <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> .
}
```
## save WURCS data

* 2022-09-26_Branch_WURCSs.tsv


* convert from WURCS to svg image.

```
<?php
$filename = '2022-09-26_Branch_WURCSs.tsv';
$lines = file($filename);
$count = 1;
foreach ($lines as $value) {
    $co = md5($value);
    $cmd = 'timeout 30s java -jar ./wurcs2pic-0.12.0-all.jar svg '.'"'.$value.'" '."./svg/".$co;
    exec($cmd);
    echo $count."\t".$co."\t".$value;
    $count++;
}
?>
```
* find empty files

`cd svg`

* 1760 entries.
  * blank line?

```
$ ls -l | wc -l
1760
```

* 47 empty svg files.

```
$ find . -type f -empty | wc -l
47
```


