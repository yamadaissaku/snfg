# GlyTouCan

## download form https://glycosmos.org/download/

* https://glycosmos.org/download/glycosmos_glycans_wurcs.csv (September 12, 202)

* select WURCS strings
   * Sep-12-2022_glycosmos_wurcs.txt

* convert from WURCS to svg image.

```
<?php
$filename = 'Sep-12-2022_glycosmos_wurcs.txt';
$lines = file($filename);
$count = 1;
foreach ($lines as $value) {
    $co = md5($value);
    $cmd = 'timeout 30s java -jar ./wurcs2pic-0.12.0-all.jar svg '.'"'.$value.'" '."./svg/".$co;
    exec($cmd);
    echo $count."\t".$co."\t".$value;
    $count++;
}
?>
```
* find empty files

`cd svg`

* 160270 GlyTouCan entries.

```
$ ls -l | wc -l
160270
```

* 46090 GlyTouCan empty svg files.

```
$ find . -type f -empty | wc -l
46090
```


